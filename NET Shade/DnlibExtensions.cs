﻿using dnlib.DotNet;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET_Shade
{
    static class DnlibExtensions
    {     
        internal static bool IsDerived(this TypeDef type)
        {
            if (type.BaseType != null && type.BaseType.Namespace.Equals(type.Namespace) || type.HasInterfaces)
            {
                return true;
            }
            return false;
        }

        internal static bool IsForm(this TypeDef type)
        {
            if (type.BaseType != null && type.BaseType.Name == "Form")
            {
                return true;
            }
            return false;
        }

        internal static IEnumerable<string> FindAbstractMethods(this ModuleDefMD module)
        {
            foreach (TypeDef type in module.GetTypes())
            {
                if (!type.IsAbstract)
                    continue;
                foreach (MethodDef method in type.Methods)
                {
                    if (!method.IsAbstract && !method.IsVirtual)
                        continue;
                    yield return (method.FullName.Split(' ')[1]);
                }
            }
        }

        internal static IEnumerable<string> GetNamespaces(this ModuleDefMD module)
        {
            ArrayList nsList = new ArrayList();
            foreach (TypeDef type in module.GetTypes())
            {
                if (nsList.IndexOf(type.Namespace.String) == -1)
                {
                    nsList.Add(type.Namespace.String);
                }
            }
            return nsList.ToArray(typeof(string)) as string[];
        }

        internal static string GetBiggestNS(this ModuleDefMD module)
        {
            List<string> list = new List<string>();
            foreach (TypeDef type in module.GetTypes())
            {
                if (type.Namespace != "")
                {
                    list.Add(type.Namespace);
                }
            }
            return list.GroupBy(i => i).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
        }
    }
}
