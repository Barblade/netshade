﻿namespace NET_Shade
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dlgSelectAssembly = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectAssembly = new System.Windows.Forms.Button();
            this.lblAssemblyName = new System.Windows.Forms.Label();
            this.rtxtMapping = new System.Windows.Forms.RichTextBox();
            this.menuClear = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miClear = new System.Windows.Forms.ToolStripMenuItem();
            this.btnObfuscate = new System.Windows.Forms.Button();
            this.cbRenaming = new System.Windows.Forms.CheckBox();
            this.gbMapping = new System.Windows.Forms.GroupBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabObfuscation = new System.Windows.Forms.TabPage();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.gbPreferences = new System.Windows.Forms.GroupBox();
            this.cbEncryption = new System.Windows.Forms.CheckBox();
            this.cbRenamingMode = new System.Windows.Forms.ComboBox();
            this.tabExclusions = new System.Windows.Forms.TabPage();
            this.lbExclusions = new System.Windows.Forms.ListBox();
            this.lblExclusions = new System.Windows.Forms.Label();
            this.btnRemoveExclusion = new System.Windows.Forms.Button();
            this.btnAddExclusion = new System.Windows.Forms.Button();
            this.tvAssembly = new System.Windows.Forms.TreeView();
            this.tabMapping = new System.Windows.Forms.TabPage();
            this.btnSaveMapping = new System.Windows.Forms.Button();
            this.tabAbout = new System.Windows.Forms.TabPage();
            this.lblAbout2 = new System.Windows.Forms.Label();
            this.lblAbout1 = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.iconList = new System.Windows.Forms.ImageList(this.components);
            this.dlgSaveMappings = new System.Windows.Forms.SaveFileDialog();
            this.cbAntiTamper = new System.Windows.Forms.CheckBox();
            this.cbAntiIldasm = new System.Windows.Forms.CheckBox();
            this.lblSavedAssembly = new System.Windows.Forms.Label();
            this.menuClear.SuspendLayout();
            this.gbMapping.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabObfuscation.SuspendLayout();
            this.gbPreferences.SuspendLayout();
            this.tabExclusions.SuspendLayout();
            this.tabMapping.SuspendLayout();
            this.tabAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // dlgSelectAssembly
            // 
            this.dlgSelectAssembly.FileName = "openAssemblyDialog";
            // 
            // btnSelectAssembly
            // 
            this.btnSelectAssembly.Location = new System.Drawing.Point(6, 9);
            this.btnSelectAssembly.Name = "btnSelectAssembly";
            this.btnSelectAssembly.Size = new System.Drawing.Size(101, 23);
            this.btnSelectAssembly.TabIndex = 0;
            this.btnSelectAssembly.Text = "Select Assembly";
            this.btnSelectAssembly.UseVisualStyleBackColor = true;
            this.btnSelectAssembly.Click += new System.EventHandler(this.selectAssemblyBtn_Click);
            // 
            // lblAssemblyName
            // 
            this.lblAssemblyName.AutoSize = true;
            this.lblAssemblyName.Location = new System.Drawing.Point(8, 35);
            this.lblAssemblyName.Name = "lblAssemblyName";
            this.lblAssemblyName.Size = new System.Drawing.Size(0, 13);
            this.lblAssemblyName.TabIndex = 1;
            // 
            // rtxtMapping
            // 
            this.rtxtMapping.ContextMenuStrip = this.menuClear;
            this.rtxtMapping.Location = new System.Drawing.Point(7, 19);
            this.rtxtMapping.Name = "rtxtMapping";
            this.rtxtMapping.ReadOnly = true;
            this.rtxtMapping.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtxtMapping.Size = new System.Drawing.Size(300, 288);
            this.rtxtMapping.TabIndex = 2;
            this.rtxtMapping.Text = "";
            this.rtxtMapping.WordWrap = false;
            // 
            // menuClear
            // 
            this.menuClear.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miClear});
            this.menuClear.Name = "clearMenu";
            this.menuClear.Size = new System.Drawing.Size(102, 26);
            // 
            // miClear
            // 
            this.miClear.Name = "miClear";
            this.miClear.Size = new System.Drawing.Size(101, 22);
            this.miClear.Text = "Clear";
            this.miClear.Click += new System.EventHandler(this.clearMenuItem_Click);
            // 
            // btnObfuscate
            // 
            this.btnObfuscate.Location = new System.Drawing.Point(242, 182);
            this.btnObfuscate.Name = "btnObfuscate";
            this.btnObfuscate.Size = new System.Drawing.Size(75, 23);
            this.btnObfuscate.TabIndex = 3;
            this.btnObfuscate.Text = "Obfuscate";
            this.btnObfuscate.UseVisualStyleBackColor = true;
            this.btnObfuscate.Click += new System.EventHandler(this.obfuscateBtn_Click);
            // 
            // cbRenaming
            // 
            this.cbRenaming.AutoSize = true;
            this.cbRenaming.Location = new System.Drawing.Point(6, 22);
            this.cbRenaming.Name = "cbRenaming";
            this.cbRenaming.Size = new System.Drawing.Size(106, 17);
            this.cbRenaming.TabIndex = 4;
            this.cbRenaming.Text = "Symbol renaming";
            this.cbRenaming.UseVisualStyleBackColor = true;
            // 
            // gbMapping
            // 
            this.gbMapping.Controls.Add(this.rtxtMapping);
            this.gbMapping.Location = new System.Drawing.Point(6, 6);
            this.gbMapping.Name = "gbMapping";
            this.gbMapping.Size = new System.Drawing.Size(312, 313);
            this.gbMapping.TabIndex = 5;
            this.gbMapping.TabStop = false;
            this.gbMapping.Text = "Mapping view";
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabObfuscation);
            this.tabControlMain.Controls.Add(this.tabExclusions);
            this.tabControlMain.Controls.Add(this.tabMapping);
            this.tabControlMain.Controls.Add(this.tabAbout);
            this.tabControlMain.Location = new System.Drawing.Point(3, 3);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(332, 393);
            this.tabControlMain.TabIndex = 6;
            // 
            // tabObfuscation
            // 
            this.tabObfuscation.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabObfuscation.Controls.Add(this.lblSavedAssembly);
            this.tabObfuscation.Controls.Add(this.progressBar);
            this.tabObfuscation.Controls.Add(this.gbPreferences);
            this.tabObfuscation.Controls.Add(this.lblAssemblyName);
            this.tabObfuscation.Controls.Add(this.btnSelectAssembly);
            this.tabObfuscation.Controls.Add(this.btnObfuscate);
            this.tabObfuscation.Location = new System.Drawing.Point(4, 22);
            this.tabObfuscation.Name = "tabObfuscation";
            this.tabObfuscation.Padding = new System.Windows.Forms.Padding(3);
            this.tabObfuscation.Size = new System.Drawing.Size(324, 367);
            this.tabObfuscation.TabIndex = 0;
            this.tabObfuscation.Text = "Obfuscation";
            // 
            // progressBar
            // 
            this.progressBar.BackColor = System.Drawing.SystemColors.Control;
            this.progressBar.Location = new System.Drawing.Point(6, 345);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(312, 16);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 6;
            this.progressBar.Visible = false;
            // 
            // gbPreferences
            // 
            this.gbPreferences.Controls.Add(this.cbAntiTamper);
            this.gbPreferences.Controls.Add(this.cbAntiIldasm);
            this.gbPreferences.Controls.Add(this.cbEncryption);
            this.gbPreferences.Controls.Add(this.cbRenamingMode);
            this.gbPreferences.Controls.Add(this.cbRenaming);
            this.gbPreferences.Enabled = false;
            this.gbPreferences.Location = new System.Drawing.Point(6, 53);
            this.gbPreferences.Name = "gbPreferences";
            this.gbPreferences.Size = new System.Drawing.Size(312, 123);
            this.gbPreferences.TabIndex = 5;
            this.gbPreferences.TabStop = false;
            this.gbPreferences.Text = "Preferences";
            // 
            // cbEncryption
            // 
            this.cbEncryption.AutoSize = true;
            this.cbEncryption.Location = new System.Drawing.Point(6, 45);
            this.cbEncryption.Name = "cbEncryption";
            this.cbEncryption.Size = new System.Drawing.Size(105, 17);
            this.cbEncryption.TabIndex = 6;
            this.cbEncryption.Text = "String encryption";
            this.cbEncryption.UseVisualStyleBackColor = true;
            // 
            // cbRenamingMode
            // 
            this.cbRenamingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRenamingMode.FormattingEnabled = true;
            this.cbRenamingMode.Location = new System.Drawing.Point(218, 22);
            this.cbRenamingMode.Name = "cbRenamingMode";
            this.cbRenamingMode.Size = new System.Drawing.Size(88, 21);
            this.cbRenamingMode.TabIndex = 5;
            // 
            // tabExclusions
            // 
            this.tabExclusions.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabExclusions.Controls.Add(this.lbExclusions);
            this.tabExclusions.Controls.Add(this.lblExclusions);
            this.tabExclusions.Controls.Add(this.btnRemoveExclusion);
            this.tabExclusions.Controls.Add(this.btnAddExclusion);
            this.tabExclusions.Controls.Add(this.tvAssembly);
            this.tabExclusions.Location = new System.Drawing.Point(4, 22);
            this.tabExclusions.Name = "tabExclusions";
            this.tabExclusions.Padding = new System.Windows.Forms.Padding(3);
            this.tabExclusions.Size = new System.Drawing.Size(324, 367);
            this.tabExclusions.TabIndex = 1;
            this.tabExclusions.Text = "Exclusions";
            // 
            // lbExclusions
            // 
            this.lbExclusions.ContextMenuStrip = this.menuClear;
            this.lbExclusions.FormattingEnabled = true;
            this.lbExclusions.Location = new System.Drawing.Point(6, 278);
            this.lbExclusions.Name = "lbExclusions";
            this.lbExclusions.Size = new System.Drawing.Size(312, 82);
            this.lbExclusions.TabIndex = 5;
            // 
            // lblExclusions
            // 
            this.lblExclusions.AutoSize = true;
            this.lblExclusions.Location = new System.Drawing.Point(131, 259);
            this.lblExclusions.Name = "lblExclusions";
            this.lblExclusions.Size = new System.Drawing.Size(71, 13);
            this.lblExclusions.TabIndex = 4;
            this.lblExclusions.Text = "Exclusion List";
            // 
            // btnRemoveExclusion
            // 
            this.btnRemoveExclusion.Location = new System.Drawing.Point(243, 249);
            this.btnRemoveExclusion.Name = "btnRemoveExclusion";
            this.btnRemoveExclusion.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveExclusion.TabIndex = 3;
            this.btnRemoveExclusion.Text = "Remove";
            this.btnRemoveExclusion.UseVisualStyleBackColor = true;
            this.btnRemoveExclusion.Click += new System.EventHandler(this.removeExclBtn_Click);
            // 
            // btnAddExclusion
            // 
            this.btnAddExclusion.Location = new System.Drawing.Point(6, 249);
            this.btnAddExclusion.Name = "btnAddExclusion";
            this.btnAddExclusion.Size = new System.Drawing.Size(75, 23);
            this.btnAddExclusion.TabIndex = 2;
            this.btnAddExclusion.Text = "Add";
            this.btnAddExclusion.UseVisualStyleBackColor = true;
            this.btnAddExclusion.Click += new System.EventHandler(this.addExclBtn_Click);
            // 
            // tvAssembly
            // 
            this.tvAssembly.ContextMenuStrip = this.menuClear;
            this.tvAssembly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tvAssembly.Location = new System.Drawing.Point(6, 6);
            this.tvAssembly.Name = "tvAssembly";
            this.tvAssembly.Size = new System.Drawing.Size(312, 237);
            this.tvAssembly.TabIndex = 0;
            // 
            // tabMapping
            // 
            this.tabMapping.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabMapping.Controls.Add(this.btnSaveMapping);
            this.tabMapping.Controls.Add(this.gbMapping);
            this.tabMapping.Location = new System.Drawing.Point(4, 22);
            this.tabMapping.Name = "tabMapping";
            this.tabMapping.Padding = new System.Windows.Forms.Padding(3);
            this.tabMapping.Size = new System.Drawing.Size(324, 367);
            this.tabMapping.TabIndex = 2;
            this.tabMapping.Text = "Mapping";
            // 
            // btnSaveMapping
            // 
            this.btnSaveMapping.Location = new System.Drawing.Point(243, 325);
            this.btnSaveMapping.Name = "btnSaveMapping";
            this.btnSaveMapping.Size = new System.Drawing.Size(75, 23);
            this.btnSaveMapping.TabIndex = 6;
            this.btnSaveMapping.Text = "Save";
            this.btnSaveMapping.UseVisualStyleBackColor = true;
            this.btnSaveMapping.Click += new System.EventHandler(this.saveMappings_Click);
            // 
            // tabAbout
            // 
            this.tabAbout.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabAbout.Controls.Add(this.lblAbout2);
            this.tabAbout.Controls.Add(this.lblAbout1);
            this.tabAbout.Controls.Add(this.picLogo);
            this.tabAbout.Location = new System.Drawing.Point(4, 22);
            this.tabAbout.Name = "tabAbout";
            this.tabAbout.Padding = new System.Windows.Forms.Padding(3);
            this.tabAbout.Size = new System.Drawing.Size(324, 367);
            this.tabAbout.TabIndex = 3;
            this.tabAbout.Text = "About";
            // 
            // lblAbout2
            // 
            this.lblAbout2.AutoSize = true;
            this.lblAbout2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAbout2.Location = new System.Drawing.Point(190, 171);
            this.lblAbout2.Name = "lblAbout2";
            this.lblAbout2.Size = new System.Drawing.Size(68, 15);
            this.lblAbout2.TabIndex = 3;
            this.lblAbout2.Text = "Version 1.0";
            // 
            // lblAbout1
            // 
            this.lblAbout1.AutoSize = true;
            this.lblAbout1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAbout1.Location = new System.Drawing.Point(190, 141);
            this.lblAbout1.Name = "lblAbout1";
            this.lblAbout1.Size = new System.Drawing.Size(70, 15);
            this.lblAbout1.TabIndex = 2;
            this.lblAbout1.Text = "NET Shade";
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.InitialImage = null;
            this.picLogo.Location = new System.Drawing.Point(24, 103);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(128, 128);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // iconList
            // 
            this.iconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconList.ImageStream")));
            this.iconList.TransparentColor = System.Drawing.Color.Transparent;
            this.iconList.Images.SetKeyName(0, "module.png");
            this.iconList.Images.SetKeyName(1, "namespace.png");
            this.iconList.Images.SetKeyName(2, "class.png");
            this.iconList.Images.SetKeyName(3, "method.png");
            this.iconList.Images.SetKeyName(4, "field.png");
            this.iconList.Images.SetKeyName(5, "property.png");
            this.iconList.Images.SetKeyName(6, "event.png");
            this.iconList.Images.SetKeyName(7, "enum.png");
            this.iconList.Images.SetKeyName(8, "struct.png");
            this.iconList.Images.SetKeyName(9, "interface.png");
            // 
            // cbAntiTamper
            // 
            this.cbAntiTamper.AutoSize = true;
            this.cbAntiTamper.Location = new System.Drawing.Point(7, 91);
            this.cbAntiTamper.Name = "cbAntiTamper";
            this.cbAntiTamper.Size = new System.Drawing.Size(97, 17);
            this.cbAntiTamper.TabIndex = 8;
            this.cbAntiTamper.Text = "Anti-Tampering";
            this.cbAntiTamper.UseVisualStyleBackColor = true;
            // 
            // cbAntiIldasm
            // 
            this.cbAntiIldasm.AutoSize = true;
            this.cbAntiIldasm.Location = new System.Drawing.Point(7, 68);
            this.cbAntiIldasm.Name = "cbAntiIldasm";
            this.cbAntiIldasm.Size = new System.Drawing.Size(87, 17);
            this.cbAntiIldasm.TabIndex = 7;
            this.cbAntiIldasm.Text = "Anti-ILDASM";
            this.cbAntiIldasm.UseVisualStyleBackColor = true;
            // 
            // lblSavedAssembly
            // 
            this.lblSavedAssembly.AutoSize = true;
            this.lblSavedAssembly.Location = new System.Drawing.Point(8, 345);
            this.lblSavedAssembly.Name = "lblSavedAssembly";
            this.lblSavedAssembly.Size = new System.Drawing.Size(0, 13);
            this.lblSavedAssembly.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(336, 398);
            this.Controls.Add(this.tabControlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "NET Shade";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuClear.ResumeLayout(false);
            this.gbMapping.ResumeLayout(false);
            this.tabControlMain.ResumeLayout(false);
            this.tabObfuscation.ResumeLayout(false);
            this.tabObfuscation.PerformLayout();
            this.gbPreferences.ResumeLayout(false);
            this.gbPreferences.PerformLayout();
            this.tabExclusions.ResumeLayout(false);
            this.tabExclusions.PerformLayout();
            this.tabMapping.ResumeLayout(false);
            this.tabAbout.ResumeLayout(false);
            this.tabAbout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog dlgSelectAssembly;
        private System.Windows.Forms.Button btnSelectAssembly;
        private System.Windows.Forms.Label lblAssemblyName;
        private System.Windows.Forms.RichTextBox rtxtMapping;
        private System.Windows.Forms.Button btnObfuscate;
        private System.Windows.Forms.CheckBox cbRenaming;
        private System.Windows.Forms.GroupBox gbMapping;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabObfuscation;
        private System.Windows.Forms.GroupBox gbPreferences;
        private System.Windows.Forms.TabPage tabExclusions;
        private System.Windows.Forms.TabPage tabMapping;
        private System.Windows.Forms.TreeView tvAssembly;
        private System.Windows.Forms.ImageList iconList;
        private System.Windows.Forms.TabPage tabAbout;
        private System.Windows.Forms.ComboBox cbRenamingMode;
        private System.Windows.Forms.Button btnSaveMapping;
        private System.Windows.Forms.Button btnRemoveExclusion;
        private System.Windows.Forms.Button btnAddExclusion;
        private System.Windows.Forms.Label lblExclusions;
        private System.Windows.Forms.ListBox lbExclusions;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.SaveFileDialog dlgSaveMappings;
        private System.Windows.Forms.ContextMenuStrip menuClear;
        private System.Windows.Forms.ToolStripMenuItem miClear;
        private System.Windows.Forms.CheckBox cbEncryption;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label lblAbout1;
        private System.Windows.Forms.Label lblAbout2;
        private System.Windows.Forms.CheckBox cbAntiTamper;
        private System.Windows.Forms.CheckBox cbAntiIldasm;
        private System.Windows.Forms.Label lblSavedAssembly;
    }
}

