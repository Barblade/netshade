﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NET_Shade
{
    static class Encryption
    {
        public static string Encrypt(string str)
        {
            if (String.IsNullOrEmpty(str))
                return null;

            DES des = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(des.Key, des.IV), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(str);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            string result = Convert.ToBase64String(des.Key) + Convert.ToBase64String(des.IV) + Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
            char[] resultArray = result.ToCharArray();

            for (int i = 0; i < result.Length - 2; i++)
            {
                resultArray[i] = (char)(resultArray[i] + resultArray[result.Length - 2]);
            }
            return new string(resultArray);
        }

        public static string Decrypt(string str)
        {
            if (String.IsNullOrEmpty(str))
                return null;

            char[] strArray = str.ToCharArray();
            for (int i = 0; i < str.Length - 2; i++)
            {
                strArray[i] = (char)(strArray[i] - strArray[str.Length - 2]);
            }
            str = new string(strArray);
            DES des = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(str.Substring(24)));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                des.CreateDecryptor(Convert.FromBase64String(str.Substring(0, 12)), Convert.FromBase64String(str.Substring(12, 12))), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }
    }
}
