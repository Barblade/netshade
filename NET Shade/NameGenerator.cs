﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NET_Shade
{
    class NameGenerator
    {
        private Renamer.RenamingMethod method;
        private SHA256Managed sha256;
        private string symbolSet = null;
        private static Random rand;

        public NameGenerator(Renamer.RenamingMethod method)
        {
            this.method = method;

            if (method == Renamer.RenamingMethod.cryptographic)
            {
                sha256 = new SHA256Managed();
            }
            else
            {
                switch (method)
                {
                    case Renamer.RenamingMethod.japaneese:
                        symbolSet = "あいうえおかきくけこがぎぐげごさしすせそざじずぜぞたちつてとだぢづでなにぬねのはひほばびぶべぼぱぴぷぺぽまみむめもやゆよらりるれろわをん";
                        break;
                    case Renamer.RenamingMethod.normal:
                        symbolSet = "abcdefghijklmnopqrstuwxyz";
                        break;
                    case Renamer.RenamingMethod.nonprintable:
                        symbolSet = "\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u2000C";
                        break;
                }

                rand = rand = new Random();
            }
        }

        public string NewName(string name = "")
        {
            MainForm.logger.log(name + " => ");

            if (string.IsNullOrEmpty(symbolSet))
            {
                byte[] hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(name), 0, name.Length);
                name = Convert.ToBase64String(hashBytes);
            }
            else
            {         
                name = "";

                for (int i=0; i<8; i++)
                {
                    name += symbolSet[rand.Next(0, symbolSet.Length - 1)];
                }
            }
            MainForm.logger.log(name + "\n");

            return name;
        }
    }
}
