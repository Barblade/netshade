﻿using dnlib.DotNet;
using System;
using System.Windows.Forms;

namespace NET_Shade
{
    class AssemblyManager
    {
        public byte[] ModuleData { get; set; }
        public ModuleDefMD Module { get; set; }

        private string inputPath;

        public AssemblyManager(string path)
        {
            inputPath = path;
        }

        public bool LoadAssembly()
        {
            try
            {
                ModuleData = System.IO.File.ReadAllBytes(inputPath);
                Module = ModuleDefMD.Load(ModuleData);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message + "\nDid you choose proper .NET assembly?", "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        public void reloadModule()
        {
            Module = ModuleDefMD.Load(ModuleData);
        }

        public bool SaveAssembly(string path)
        {
            try
            {
                if (Module.IsILOnly)
                {
                    Module.Write(path);
                }
                else
                {
                    Module.NativeWrite(path);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
    }
}
