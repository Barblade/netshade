﻿using System.Collections;
using System.Windows.Forms;

namespace NET_Shade
{
    class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            TreeNode tx = x as TreeNode;
            TreeNode ty = y as TreeNode;
            
            if (string.IsNullOrEmpty(tx.Name))
                return (tx.Text.CompareTo(ty.Text));
            else
                return (tx.Name.CompareTo(ty.Name));
        }
    }
}
