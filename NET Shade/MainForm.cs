﻿using dnlib.DotNet;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace NET_Shade
{
    public partial class MainForm : Form
    {
        private static AssemblyManager asmManager;
        private ContentLoader contentLoader;
        internal static Logger logger;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbRenamingMode.DataSource = Enum.GetValues(typeof(Renamer.RenamingMethod));
            cbRenamingMode.SelectedIndex = 0;
            ProgressDisplayer.ProgressBar = progressBar;
            ((Control)tabExclusions).Enabled = false;
            ((Control)tabMapping).Enabled = false;
            Verifier.ExclusionBox = lbExclusions;
        }

        private void selectAssemblyBtn_Click(object sender, EventArgs e)
        {
            if (dlgSelectAssembly.ShowDialog() == DialogResult.OK)
            {
                asmManager = new AssemblyManager(dlgSelectAssembly.FileName.ToString());
                if (asmManager.LoadAssembly() == false)
                    return;
                lblAssemblyName.Text = dlgSelectAssembly.FileName;
                contentLoader = new ContentLoader(asmManager.Module, iconList);
                contentLoader.loadTypes(tvAssembly);
                ((Control)tabExclusions).Enabled = true;
                lbExclusions.Items.Clear();
                gbPreferences.Enabled = true;
                Preferences preferenes = (Preferences)IsolatedStorage.Load(asmManager.Module.Name + ".json");
                if (preferenes != null)
                {
                    cbRenaming.Checked = preferenes.symbolRenaming;
                    cbRenamingMode.SelectedItem = preferenes.renamingMethod;
                    cbEncryption.Checked = preferenes.stringEncryption;
                    cbAntiIldasm.Checked = preferenes.antiILDASM;
                    cbAntiTamper.Checked = preferenes.antiTampering;
                    lbExclusions.Items.AddRange(preferenes.exclusions);
                    lblSavedAssembly.Text = "";
                }
            }
        }

        private void obfuscateBtn_Click(object sender, EventArgs e)
        {
            Renamer.RenamingMethod chosenMethod;
            Preferences preferences = new Preferences();
            AntiTampering antiTamper  = null;
            lblSavedAssembly.Text = "";
            string filePath = dlgSelectAssembly.FileName;
            filePath = filePath.Substring(0, filePath.LastIndexOf(".")) + "-obf" + filePath.Substring(filePath.LastIndexOf("."));

            if (asmManager != null && asmManager.Module != null)
            {
                logger = new Logger(rtxtMapping);

                if (cbEncryption.Checked)
                {
                    MethodDef decryptMethod = Injector.InjectDecryption(asmManager.Module, asmManager.Module.GetBiggestNS());
                    Encrypter encrypter = new Encrypter(asmManager.Module, decryptMethod);
                    preferences.stringEncryption = true;
                    encrypter.EncryptStrings();
                }

                if (cbAntiTamper.Checked)
                {
                    MethodDef antiTamperMethod = Injector.InjectAntiTampering(asmManager.Module, asmManager.Module.GetBiggestNS());
                    antiTamper = new AntiTampering(asmManager.Module, antiTamperMethod);
                    antiTamper.AddAntiTamperCall();
                    preferences.antiTampering = true;
                }

                if (cbRenaming.Checked)
                {
                    Enum.TryParse(cbRenamingMode.SelectedValue.ToString(), out chosenMethod);
                    Renamer renamer = new Renamer(asmManager.Module, chosenMethod);
                    preferences.renamingMethod = chosenMethod;
                    preferences.symbolRenaming = true;
                    renamer.StartRenaming();
                }

                if (cbAntiIldasm.Checked)
                {
                    preferences.antiILDASM = true;
                    AntiILDASM.applyAntiILDASM(asmManager.Module);
                }
               
                if (asmManager.SaveAssembly(filePath))
                {
                    if (cbAntiTamper.Checked)
                        antiTamper.AppendHash(filePath);
                    lblSavedAssembly.Text = "Saved as " + filePath.Substring(filePath.LastIndexOf('\\') + 1);
                    preferences.exclusions = lbExclusions.Items.Cast<string>().ToArray();
                    IsolatedStorage.Save(preferences, asmManager.Module.Name + ".json");
                    ((Control)tabMapping).Enabled = true;
                }
                asmManager.reloadModule();
            }
        }

        private void addExclBtn_Click(object sender, EventArgs e)
        {
            if (tvAssembly.SelectedNode != null)
            {
                string exclusion = tvAssembly.SelectedNode.FullPath.Replace('\\', '.')
                        .Substring(tvAssembly.SelectedNode.FullPath.IndexOf('\\') + 1);

                if (lbExclusions.FindString(exclusion) == ListBox.NoMatches)
                    lbExclusions.Items.Add(exclusion);
            }
        }

        private void removeExclBtn_Click(object sender, EventArgs e)
        {
            if (lbExclusions.SelectedIndex >= 0)
            {
                lbExclusions.Items.Remove(lbExclusions.SelectedItem);
            }
        }

        private void saveMappings_Click(object sender, EventArgs e)
        {
            dlgSaveMappings.Filter = "Text|*.txt|All|*.*";
            dlgSaveMappings.RestoreDirectory = true;

            if (dlgSaveMappings.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(dlgSaveMappings.FileName, rtxtMapping.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void clearMenuItem_Click(object sender, EventArgs e)
        {
            Control control = ((sender as ToolStripMenuItem).Owner as ContextMenuStrip).SourceControl;

            switch (control.GetType().Name.ToString())
            {
                case "ListBox":
                    {
                        (control as ListBox).Items.Clear();
                        return;
                    }
                case "TreeView":
                    {
                        (control as TreeView).Nodes.Clear();
                        return;
                    }
            }
        }
    }
}
