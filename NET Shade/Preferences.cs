﻿namespace NET_Shade
{
    class Preferences
    {
        public string[] exclusions { get; set; }
        public bool stringEncryption { get; set; }
        public bool symbolRenaming { get; set;  }
        public bool antiILDASM { get; set; }
        public bool antiTampering { get; set; }
        public Renamer.RenamingMethod renamingMethod { get; set; }

        public Preferences() {}
    }
}
