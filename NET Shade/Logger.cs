﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NET_Shade
{
    class Logger
    {
        internal RichTextBox MappingBox { private get; set; }

        public Logger(RichTextBox mappingBox)
        {
            MappingBox = mappingBox;
            mappingBox.Clear();
        }

        public void log(string msg)
        {
            if (msg.Length > 0)
            {
                MappingBox.AppendText(msg);
            }
        }
    }
}
