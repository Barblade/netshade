﻿using dnlib.DotNet;

namespace NET_Shade
{
    static class AntiILDASM
    {
        public static void applyAntiILDASM(ModuleDefMD module)
        {
            TypeRef attrRef = module.CorLibTypes.GetTypeRef("System.Runtime.CompilerServices", "SuppressIldasmAttribute");
            var ctorRef = new MemberRefUser(module, ".ctor", MethodSig.CreateInstance(module.CorLibTypes.Void), attrRef);
            module.CustomAttributes.Add(new CustomAttribute(ctorRef));
        }
    }
}
