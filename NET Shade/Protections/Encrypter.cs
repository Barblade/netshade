﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.Collections.Generic;

namespace NET_Shade
{
    class Encrypter
    {
        private ModuleDefMD module;
        public MethodDef decryptMethod { get; set; }

        public Encrypter(ModuleDefMD module, MethodDef decryptMethod)
        {
            this.module = module;
            this.decryptMethod = decryptMethod;
        }

        public void EncryptStrings()
        {
            List<int> ldstrIndexes = new List<int>();
           
            foreach (TypeDef type in module.GetTypes())
            {
                foreach (MethodDef method in type.Methods)
                {
                    if (!method.HasBody || !method.Body.HasInstructions)
                        continue;
                    int i = 0;
                    foreach (Instruction instruction in method.Body.Instructions)
                    {
                        if (instruction.OpCode.Code == Code.Ldstr && instruction.Operand != null && (string)instruction.Operand != String.Empty)
                        {
                            ldstrIndexes.Add(i);
                            instruction.Operand = Encryption.Encrypt(instruction.Operand.ToString());
                        }
                        i++;
                    }
                    ldstrIndexes.Reverse();
                    if (ldstrIndexes.Count <= 0)
                        continue;
                    foreach(int j in ldstrIndexes)
                        method.Body.Instructions.Insert(j + 1, new Instruction(OpCodes.Call, decryptMethod));
                    method.Body.SimplifyBranches();
                    method.Body.OptimizeBranches();
                    ldstrIndexes.Clear();
                }
            }
        }
    }
}
