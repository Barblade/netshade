﻿using dnlib.DotNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET_Shade
{
    class Renamer
    {
        private ModuleDefMD module;
        private NameGenerator nameGenerator;
        private Dictionary<string, string> abstractMethods;
        private Dictionary<string, string> nsDictionary;

        public enum RenamingMethod
        {
            normal,
            cryptographic,
            japaneese,
            nonprintable
        }

        public Renamer(ModuleDefMD module, RenamingMethod method)
        {
            this.module = module;
            nameGenerator = new NameGenerator(method);
        }

        public void StartRenaming()
        {
            ProgressDisplayer renamingProgress = new ProgressDisplayer(100.0 / module.GetTypes().Count());

            nsDictionary = module.GetNamespaces()
                .ToDictionary(name => name, newName => nameGenerator.NewName(newName));

            abstractMethods = module.FindAbstractMethods()
                .ToDictionary(fullName => fullName, newName => nameGenerator
                .NewName(newName.Split('.').Last()));

            foreach (TypeDef type in module.GetTypes())
            {
                renamingProgress.updateProgress();

                if (!Verifier.VerifyType(type) || Verifier.isExclusion(type.FullName))
                    continue;
                RenameSymbols(type);
            }

            foreach (TypeDef type in module.GetTypes())
            {
                string originalTypeName = type.Name;
                string originalNsName = type.Namespace;

                if (Verifier.isExclusion(type.Namespace))
                    continue;
                type.Namespace = nsDictionary[type.Namespace];
                if (!Verifier.VerifyType(type) || Verifier.isExclusion(type.FullName))
                    continue;
                type.Name = nameGenerator.NewName(type.Name);

                if (type.IsForm())
                {
                    foreach (Resource resource in module.Resources)
                    {
                        string[] parts = resource.Name.ToString().Split('.');
                        if (parts[1] != null && parts[0].Equals(originalNsName) && parts[1].Equals(originalTypeName))
                        {
                            parts[0] = type.Namespace;
                            parts[1] = type.Name;
                            resource.Name = String.Join(".", parts);
                        }
                    }
                }
            }
        }
        private void RenameSymbols(TypeDef type)
        {
            RenameMethods(type);
            RenameFields(type);
            RenameProperties(type);
            RenameEvents(type);
            RenameNestedTypes(type);
        }
        private void RenameMethods(TypeDef type)
        {
            foreach (MethodDef method in type.Methods)
            {
                string newMethodName = method.Name;
                string fullName;
                ArrayList possibleBaseMethods = new ArrayList();

                if (type.IsDerived() || method.IsAbstract || method.IsVirtual)
                {
                    foreach (InterfaceImpl iface in type.Interfaces)
                    {
                        fullName = type.Namespace + '.' + iface.Interface.Name + "::" + method.FullName.Split(':')[2];
                        possibleBaseMethods.Add(fullName);
                    }

                    if (type.BaseType != null)
                    {
                        fullName = type.BaseType.FullName + "::" + method.FullName.Split(':')[2];
                        possibleBaseMethods.Add(fullName);
                    }
                    fullName = method.FullName.Split(' ')[1];
                    possibleBaseMethods.Add(fullName);

                    foreach (string possibleBase in possibleBaseMethods)
                    {
                        if (abstractMethods.ContainsKey(possibleBase))
                        {
                            newMethodName = abstractMethods[possibleBase];
                            break;
                        }
                    }
                }
                if (newMethodName.Equals(method.Name) && !method.IsVirtual && Verifier.VerifyMethod(method) && !Verifier.isExclusion(method.FullName))
                    newMethodName = nameGenerator.NewName(method.Name);
                method.Name = newMethodName;

                RenameParameters(method);
            }
        }
        private void RenameFields(TypeDef type)
        {
            foreach (FieldDef field in type.Fields)
            {
                if (Verifier.VerifyField(field) && !Verifier.isExclusion(field.FullName))
                {
                    field.Name = nameGenerator.NewName(field.Name);
                }
            }
        }
        private void RenameProperties(TypeDef type)
        {
            foreach (PropertyDef property in type.Properties)
            {
                if (Verifier.VerifyProperty(property) && !Verifier.isExclusion(property.FullName))
                {
                    property.Name = nameGenerator.NewName(property.Name);
                }
            }
        }
        private void RenameEvents(TypeDef type)
        {
            foreach (EventDef evnt in type.Events)
            {
                if (Verifier.VerifyEvent(evnt) && !Verifier.isExclusion(evnt.FullName))
                {
                    evnt.Name = nameGenerator.NewName(evnt.Name);
                }
            }
        }
        private void RenameNestedTypes(TypeDef type)
        {
            foreach (TypeDef nestedType in type.NestedTypes)
            {
                if (Verifier.VerifyNestedType(nestedType) && !Verifier.isExclusion(nestedType.FullName))
                {
                    RenameSymbols(nestedType);
                    nestedType.Name = nameGenerator.NewName(nestedType.Name);
                }
            }
        }
        private void RenameParameters(MethodDef method)
        {
            foreach (Parameter param in method.Parameters)
            {
                if (param.IsNormalMethodParameter && !param.IsHiddenThisParameter)
                {
                    param.Name = nameGenerator.NewName(param.Name);
                }
            }
        }
    }
}