﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System;
using System.IO;
using System.Security.Cryptography;

namespace NET_Shade
{
    class AntiTampering
    {
        private ModuleDefMD module;
        private MethodDef antiTamperMethod;

        public AntiTampering(ModuleDefMD module, MethodDef antiTamperMethod)
        {
            this.module = module;
            this.antiTamperMethod = antiTamperMethod;
        }

        public void AddAntiTamperCall()
        {
            MethodDef cctor = module.GlobalType.FindOrCreateStaticConstructor();
            cctor.Body.Instructions.Insert(0, new Instruction(OpCodes.Call, antiTamperMethod));

            foreach (MethodDef md in module.GlobalType.Methods)
            {
                if (md.Name == ".ctor")
                {
                    module.GlobalType.Remove(md);
                    break;
                }
            }
        }

        public void AppendHash(string path)
        {
            SHA256Managed sha256 = new SHA256Managed();
            byte[] hashBytes = sha256.ComputeHash(File.ReadAllBytes(path));

            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(hashBytes, 0, hashBytes.Length);
            }
        }

        public void IsTampered()
        {
            string assemblyLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            Stream stream = new StreamReader(assemblyLocation).BaseStream;
            BinaryReader reader = new BinaryReader(stream);           
            string originalHash = BitConverter.ToString(SHA256.Create().ComputeHash(reader.ReadBytes(File.ReadAllBytes(assemblyLocation).Length - 32)));
            stream.Seek(-32, SeekOrigin.End);
            if (originalHash != BitConverter.ToString(reader.ReadBytes(32)))
                System.Windows.Forms.Application.Exit();
        }
    }
}
