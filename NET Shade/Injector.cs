﻿using dnlib.DotNet;
using dnlib.DotNet.Emit;
using System.Windows.Forms;
using System.Linq;
using System;
using dnlib.DotNet.Writer;

namespace NET_Shade
{
    static class Injector
    {
        public static MethodDef InjectDecryption(ModuleDefMD module, string ns)
        {
            TypeDef decryptionClass = new TypeDefUser(ns, "Decryption",
                               module.CorLibTypes.Object.TypeDefOrRef);
            decryptionClass.Attributes = TypeAttributes.AutoLayout | TypeAttributes.Class | TypeAttributes.AnsiClass
                | TypeAttributes.Abstract | TypeAttributes.Sealed | TypeAttributes.BeforeFieldInit;
            module.Types.Add(decryptionClass);

            MethodImplAttributes methodImplFlags = MethodImplAttributes.IL | MethodImplAttributes.Managed;
            MethodAttributes methodFlags = MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig;
            MethodDef decryptMethod = new MethodDefUser("Decrypt", MethodSig.CreateStatic(module.CorLibTypes.String, module.CorLibTypes.String),
                methodImplFlags, methodFlags);
            decryptMethod.ParamDefs.Add(new ParamDefUser("str", 1));
            decryptionClass.Methods.Add(decryptMethod);

            CilBody cilBody = new CilBody();
            decryptMethod.Body = cilBody;

            Importer importer = new Importer(module);
            var charArray = importer.ImportAsTypeSig(typeof(System.Char[]));
            var stringArray = importer.ImportAsTypeSig(typeof(System.String[]));
            var byteArray = importer.ImportAsTypeSig(typeof(System.Byte[]));
            var DES = importer.ImportAsTypeSig(typeof(System.Security.Cryptography.DES));
            Local loopInt = new Local(module.CorLibTypes.Int32);
            Local strArray = new Local(stringArray);

            cilBody.Variables.Add(new Local(charArray));
            cilBody.Variables.Add(new Local(DES));
            cilBody.Variables.Add(loopInt);

            TypeRef stringRef = new TypeRefUser(module, "System", "String", module.CorLibTypes.AssemblyRef);
            TypeRef charRef = new TypeRefUser(module, "System", "Char", module.CorLibTypes.AssemblyRef);
            TypeRef desRef = new TypeRefUser(module, "System.Security.Cryptography", "DESCryptoServiceProvider", module.CorLibTypes.AssemblyRef);
            TypeRef symmAlgRef = new TypeRefUser(module, "System.Security.Cryptography", "SymmetricAlgorithm", module.CorLibTypes.AssemblyRef);
            TypeRef convRef = new TypeRefUser(module, "System", "Convert", module.CorLibTypes.AssemblyRef);
            TypeRef memoryStreamRef = new TypeRefUser(module, "System.IO", "MemoryStream", module.CorLibTypes.AssemblyRef);
            TypeRef streamReaderRef = new TypeRefUser(module, "System.IO", "StreamReader", module.CorLibTypes.AssemblyRef);
            TypeRef streamRef = new TypeRefUser(module, "System.IO", "Stream", module.CorLibTypes.AssemblyRef);
            TypeRef textReaderRef = new TypeRefUser(module, "System.IO", "TextReader", module.CorLibTypes.AssemblyRef);
            TypeRef cryptoRef = new TypeRefUser(module, "System.Security.Cryptography", "ICryptoTransform", module.CorLibTypes.AssemblyRef);
            TypeRef cryptoStreamRef = new TypeRefUser(module, "System.Security.Cryptography", "CryptoStream", module.CorLibTypes.AssemblyRef);
            TypeRef cryptoStreamModeRef = new TypeRefUser(module, "System.Security.Cryptography", "CryptoStreamMode", module.CorLibTypes.AssemblyRef);

            TypeSig cryptoSig = cryptoRef.ToTypeSig();
            TypeSig streamSig = streamRef.ToTypeSig();
            ValueTypeSig crypoStreamMode = new ValueTypeSig(cryptoStreamModeRef);

            MemberRefUser callIsNullOrEmpty = new MemberRefUser(module, "IsNullOrEmpty",
                MethodSig.CreateStatic(module.CorLibTypes.Boolean, module.CorLibTypes.String), stringRef);
            MemberRefUser callFromBase64String = new MemberRefUser(module, "FromBase64String",
                MethodSig.CreateStatic(byteArray, module.CorLibTypes.String), convRef);
            MemberRefUser callConcat = new MemberRefUser(module, "Concat",
               MethodSig.CreateStatic(module.CorLibTypes.String, module.CorLibTypes.String, module.CorLibTypes.String), stringRef);

            MemberRefUser callVirtToCharArray = new MemberRefUser(module, "ToCharArray", MethodSig.CreateInstance(charArray), stringRef);
            MemberRefUser callVirtGetLength = new MemberRefUser(module, "get_Length", MethodSig.CreateInstance(module.CorLibTypes.Int32), stringRef);
            MemberRefUser callVirtSubstring = new MemberRefUser(module, "Substring",
                MethodSig.CreateInstance(module.CorLibTypes.String, module.CorLibTypes.Int32), stringRef);
            MemberRefUser callVirtSubstring2 = new MemberRefUser(module, "Substring",
                MethodSig.CreateInstance(module.CorLibTypes.String, module.CorLibTypes.Int32, module.CorLibTypes.Int32), stringRef);
            MemberRefUser callVirtCreateDescryptor = new MemberRefUser(module, "CreateDecryptor",
                MethodSig.CreateInstance(cryptoSig, byteArray, byteArray), symmAlgRef);
            MemberRefUser callVirtReadToEnd = new MemberRefUser(module, "ReadToEnd", MethodSig.CreateInstance(module.CorLibTypes.String), textReaderRef);

            MemberRefUser newObjString = new MemberRefUser(module, ".ctor", MethodSig.CreateInstance(module.CorLibTypes.Void, charArray), stringRef);
            MemberRefUser newObjDES = new MemberRefUser(module, ".ctor", MethodSig.CreateInstance(module.CorLibTypes.Void), desRef);
            MemberRefUser newObjMemoryStream = new MemberRefUser(module, ".ctor",
                MethodSig.CreateInstance(module.CorLibTypes.Void, byteArray), memoryStreamRef);
            MemberRefUser newObjStreamReader = new MemberRefUser(module, ".ctor", MethodSig.CreateInstance(module.CorLibTypes.Void, streamSig), streamReaderRef);
            MemberRefUser newObjCryptoStream = new MemberRefUser(module, ".ctor",
                MethodSig.CreateInstance(module.CorLibTypes.Void, streamSig, cryptoSig, crypoStreamMode), cryptoStreamRef);

            Instruction instr5 = OpCodes.Ldarg_0.ToInstruction();
            Instruction instr11 = OpCodes.Ldloc_0.ToInstruction();
            Instruction instr29 = OpCodes.Ldloc_2.ToInstruction();

            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callIsNullOrEmpty));
            cilBody.Instructions.Add(OpCodes.Brfalse_S.ToInstruction(instr5));
            cilBody.Instructions.Add(OpCodes.Ldnull.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ret.ToInstruction());
            cilBody.Instructions.Add(instr5);
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtToCharArray));
            cilBody.Instructions.Add(OpCodes.Stloc_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Stloc_2.ToInstruction());

            cilBody.Instructions.Add(OpCodes.Br_S.ToInstruction(instr29));
            cilBody.Instructions.Add(instr11);
            cilBody.Instructions.Add(OpCodes.Ldloc_2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldloc_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldloc_2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldelem_U2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldloc_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtGetLength));
            cilBody.Instructions.Add(OpCodes.Ldc_I4_2.ToInstruction());

            cilBody.Instructions.Add(OpCodes.Sub.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldelem_U2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Sub.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Conv_U2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Stelem_I2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldloc_2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_1.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Add.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Stloc_2.ToInstruction());
            cilBody.Instructions.Add(instr29);

            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtGetLength));
            cilBody.Instructions.Add(OpCodes.Ldc_I4_2.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Sub.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Blt_S.ToInstruction(instr11));
            cilBody.Instructions.Add(OpCodes.Ldloc_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjString));
            cilBody.Instructions.Add(OpCodes.Starg_S.ToInstruction(strArray));
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjDES));
            cilBody.Instructions.Add(OpCodes.Stloc_1.ToInstruction());

            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_S.ToInstruction((sbyte)0x18));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtSubstring));
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callFromBase64String));
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjMemoryStream));
            cilBody.Instructions.Add(OpCodes.Ldloc_1.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_S.ToInstruction((sbyte)12));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtSubstring2));

            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callFromBase64String));
            cilBody.Instructions.Add(OpCodes.Ldarg_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4_S.ToInstruction((sbyte)12));
            cilBody.Instructions.Add(OpCodes.Ldc_I4_S.ToInstruction((sbyte)12));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtSubstring2));
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callFromBase64String));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtCreateDescryptor));
            cilBody.Instructions.Add(OpCodes.Ldc_I4_0.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjCryptoStream));
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjStreamReader));

            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callVirtReadToEnd));
            cilBody.Instructions.Add(OpCodes.Ret.ToInstruction());

            return decryptMethod;
        }

        public static MethodDef InjectAntiTampering(ModuleDefMD module, string ns)
        {
            TypeDef antiTamperClass = new TypeDefUser(ns, "TamperProtecion",
                   module.CorLibTypes.Object.TypeDefOrRef);
            antiTamperClass.Attributes = TypeAttributes.AutoLayout | TypeAttributes.Class | TypeAttributes.AnsiClass
                | TypeAttributes.Abstract | TypeAttributes.Sealed | TypeAttributes.BeforeFieldInit;
            module.Types.Add(antiTamperClass);

            MethodImplAttributes methodImplFlags = MethodImplAttributes.IL | MethodImplAttributes.Managed;
            MethodAttributes methodFlags = MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig;
            MethodDef antiTamperMethod = new MethodDefUser("isTampered", MethodSig.CreateStatic(module.CorLibTypes.Void), methodImplFlags, methodFlags);
            antiTamperClass.Methods.Add(antiTamperMethod);

            CilBody cilBody = new CilBody();
            antiTamperMethod.Body = cilBody;

            Importer importer = new Importer(module);
            var stream = importer.ImportAsTypeSig(typeof(System.IO.Stream));
            var binaryReader = importer.ImportAsTypeSig(typeof(System.IO.BinaryReader));
            var byteArray = importer.ImportAsTypeSig(typeof(byte[]));
            var seekOrigin = importer.ImportAsTypeSig(typeof(System.IO.SeekOrigin));

            Local v0 = new Local(importer.ImportAsTypeSig(typeof(string)));
            Local v1 = new Local(stream);
            Local v2 = new Local(binaryReader);
            Local v3 = new Local(importer.ImportAsTypeSig(typeof(string)));
            Local v4 = new Local(importer.ImportAsTypeSig(typeof(bool)));

            cilBody.Variables.Add(v0);
            cilBody.Variables.Add(v1);
            cilBody.Variables.Add(v2);
            cilBody.Variables.Add(v3);
            cilBody.Variables.Add(v4);

            TypeRef assemblyRef = new TypeRefUser(module, "System.Reflection", "Assembly", module.CorLibTypes.AssemblyRef);
            TypeRef streamReaderRef = new TypeRefUser(module, "System.IO", "StreamReader", module.CorLibTypes.AssemblyRef);
            TypeRef streamRef = new TypeRefUser(module, "System.IO", "Stream", module.CorLibTypes.AssemblyRef);
            TypeRef binaryReaderRef = new TypeRefUser(module, "System.IO", "BinaryReader", module.CorLibTypes.AssemblyRef);
            TypeRef sha256Ref = new TypeRefUser(module, "System.Security.Cryptography", "SHA256", module.CorLibTypes.AssemblyRef);
            TypeRef fileRef = new TypeRefUser(module, "System.IO", "File", module.CorLibTypes.AssemblyRef);
            TypeRef hashAlghorithmmRef = new TypeRefUser(module, "System.Security.Cryptography", "HashAlgorithm", module.CorLibTypes.AssemblyRef);
            TypeRef bitConverterRef = new TypeRefUser(module, "System", "BitConverter", module.CorLibTypes.AssemblyRef);
            TypeRef stringRef = new TypeRefUser(module, "System", "String", module.CorLibTypes.AssemblyRef);
            TypeRef environmentRef = new TypeRefUser(module, "System", "Environment", module.CorLibTypes.AssemblyRef);

            TypeSig assemblySig = assemblyRef.ToTypeSig();
            TypeSig streamSig = streamRef.ToTypeSig();
            TypeSig sha256Sig = sha256Ref.ToTypeSig();

            MemberRefUser callGetExecutingAssembly = new MemberRefUser(module, "GetExecutingAssembly", MethodSig.CreateStatic(assemblySig), assemblyRef);
            MemberRefUser callCreateHA256 = new MemberRefUser(module, "Create", MethodSig.CreateStatic(sha256Sig), sha256Ref);
            MemberRefUser callReadAllByyes = new MemberRefUser(module, "ReadAllBytes", MethodSig.CreateStatic(byteArray, module.CorLibTypes.String), fileRef);
            MemberRefUser callToString = new MemberRefUser(module, "ToString", MethodSig.CreateStatic(module.CorLibTypes.String, byteArray), bitConverterRef);
            MemberRefUser callop_Inequality = new MemberRefUser(module, "op_Inequality",
                MethodSig.CreateStatic(module.CorLibTypes.Boolean, module.CorLibTypes.String, module.CorLibTypes.String), stringRef);
            MemberRefUser callExit = new MemberRefUser(module, "Exit", MethodSig.CreateStatic(module.CorLibTypes.Void, module.CorLibTypes.Int32), environmentRef);


            MemberRefUser callvirtget_Location = new MemberRefUser(module, "get_Location", MethodSig.CreateInstance(module.CorLibTypes.String), assemblyRef);
            MemberRefUser callvirtget_BaseStream = new MemberRefUser(module, "get_BaseStream", MethodSig.CreateInstance(streamSig), streamReaderRef);
            MemberRefUser callvirtSeek = new MemberRefUser(module, "Seek", 
                MethodSig.CreateInstance(module.CorLibTypes.Int64, module.CorLibTypes.Int64, seekOrigin), streamRef);
            MemberRefUser callvirtReadBytes = new MemberRefUser(module, "ReadBytes", MethodSig.CreateInstance(byteArray, module.CorLibTypes.Int32), binaryReaderRef);
            MemberRefUser callvirtComputeHash = new MemberRefUser(module, "ComputeHash", MethodSig.CreateInstance(byteArray, byteArray), hashAlghorithmmRef);


            MemberRefUser newObjStreamReader = new MemberRefUser(module, ".ctor", 
                MethodSig.CreateInstance(module.CorLibTypes.Void, module.CorLibTypes.String), streamReaderRef);
            MemberRefUser newObjbinaryReader = new MemberRefUser(module, ".ctor", MethodSig.CreateInstance(module.CorLibTypes.Void, streamSig), binaryReaderRef);

            Instruction instr39 = OpCodes.Ret.ToInstruction();

            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callGetExecutingAssembly));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtget_Location));
            cilBody.Instructions.Add(OpCodes.Stloc.ToInstruction(v0));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v0));
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjStreamReader));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtget_BaseStream));
            cilBody.Instructions.Add(OpCodes.Stloc.ToInstruction(v1));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v1));
            cilBody.Instructions.Add(OpCodes.Newobj.ToInstruction(newObjbinaryReader));
            cilBody.Instructions.Add(OpCodes.Stloc.ToInstruction(v2));

            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callCreateHA256));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v2));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v0));
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callReadAllByyes));
            cilBody.Instructions.Add(OpCodes.Ldlen.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Conv_I4.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4.ToInstruction(0x20));
            cilBody.Instructions.Add(OpCodes.Sub.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtReadBytes));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtComputeHash));

            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callToString));
            cilBody.Instructions.Add(OpCodes.Stloc.ToInstruction(v3));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v1));
            cilBody.Instructions.Add(OpCodes.Ldc_I4.ToInstruction(-0x20));
            cilBody.Instructions.Add(OpCodes.Conv_I8.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldc_I4.ToInstruction(2));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtSeek));
            cilBody.Instructions.Add(OpCodes.Pop.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v3));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v2));

            cilBody.Instructions.Add(OpCodes.Ldc_I4.ToInstruction(0x20));
            cilBody.Instructions.Add(OpCodes.Callvirt.ToInstruction(callvirtReadBytes));
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callToString));
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callop_Inequality));
            cilBody.Instructions.Add(OpCodes.Stloc.ToInstruction(v4));
            cilBody.Instructions.Add(OpCodes.Ldloc.ToInstruction(v4));
            cilBody.Instructions.Add(OpCodes.Brfalse.ToInstruction(instr39));
            cilBody.Instructions.Add(OpCodes.Ldc_I4_M1.ToInstruction());
            cilBody.Instructions.Add(OpCodes.Call.ToInstruction(callExit));
            cilBody.Instructions.Add(instr39);
            
            return antiTamperMethod;
        }
    }
}
 