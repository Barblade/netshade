﻿using dnlib.DotNet;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NET_Shade
{
    class ContentLoader
    {
        private ModuleDefMD module;
        private static ImageList iconList;

        public ContentLoader(ModuleDefMD module, ImageList imageList)
        {
            this.module = module;
            if (iconList == null)
            {
                iconList = imageList;
            }
        }
        
        public void loadTypes(TreeView assemblyTree)
        {
            ProgressDisplayer loadingTypesProgress = new ProgressDisplayer(100.0 / module.GetTypes().Count());
            assemblyTree.ImageList = iconList;
            TreeNode moduleNode = assemblyTree.Nodes.Add("a" + module.Name, module.Name, 0, 0);

            string[] nsArray = module.GetNamespaces().ToArray();
            
            foreach (string ns in nsArray)
                moduleNode.Nodes.Add(ns, ns, 1, 1);

            foreach (TypeDef type in module.GetTypes())
            {
                loadingTypesProgress.updateProgress();

                if (!Verifier.VerifyType(type))
                {
                    continue;
                }

                TreeNode nsNode = moduleNode.Nodes.Find(type.Namespace, false)[0];
                TreeNode typeNode = new TreeNode(type.Name, 2, 2);
                nsNode.Nodes.Add(typeNode);
                loadMembers(type, typeNode);
            }
            assemblyTree.TreeViewNodeSorter = new NodeSorter();
            assemblyTree.Sort();
        }

        public void loadMembers(TypeDef type, TreeNode typeNode)
        {
            foreach (MethodDef method in type.Methods)
            {
                typeNode.Nodes.Add("m" + method.FullName.Split(':')[2], method.FullName.Split(':')[2], 3, 3);
            }
            foreach (FieldDef field in type.Fields)
            {
                typeNode.Nodes.Add("f "+ field.Name, field.Name, 4, 4);
            }
            foreach (PropertyDef property in type.Properties)
            {
                typeNode.Nodes.Add("p" + property.Name, property.Name, 5, 5);
            }
            foreach (EventDef evnt in type.Events)
            {
                typeNode.Nodes.Add("e" + evnt.Name, evnt.Name, 6, 6);
            }
            foreach (TypeDef nestedType in type.NestedTypes)
            {
                if (nestedType.BaseType.Name == "Enum")
                    loadMembers(nestedType, typeNode.Nodes.Add("n" + nestedType.Name, nestedType.Name, 7, 7));
                else
                    loadMembers(nestedType, typeNode.Nodes.Add("s" + nestedType.Name, nestedType.Name, 8, 8));
            }
            foreach (InterfaceImpl iface in type.Interfaces)
            {
                typeNode.Nodes.Add("i" + iface.Interface.Name, iface.Interface.Name, 9, 9);
            }
        }
    }
}
