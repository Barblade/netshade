﻿using dnlib.DotNet;
using System.Windows.Forms;

namespace NET_Shade
{
    static class Verifier
    {
        internal static ListBox ExclusionBox { private get; set; }

        public static bool VerifyType(TypeDef type)
        {
            if (type.IsRuntimeSpecialName || type.IsSpecialName || type.IsNested)
            {
                return false;
            }
            return true;
        }
        public static bool VerifyMethod(MethodDef method)
        {
            if (method.IsConstructor || method.IsFamily || method.IsRuntimeSpecialName)
            {
                return false;
            }
            return true;
        }
        public static bool VerifyField(FieldDef field)
        {
            if (field.IsFamily || field.IsRuntimeSpecialName || field.IsSpecialName)
            {
                return false;
            }
            return true;
        }
        public static bool VerifyProperty(PropertyDef property)
        {
            if (property.IsRuntimeSpecialName || property.IsSpecialName)
            {
                return false;
            }
            return true;
        }
        public static bool VerifyEvent(EventDef evnt)
        {
            if (evnt.IsRuntimeSpecialName || evnt.IsSpecialName)
            {
                return false;
            }
            return true;
        }
        public static bool VerifyNestedType(TypeDef nestedType)
        {
            if (nestedType.IsRuntimeSpecialName || nestedType.IsSpecialName)
            {
                return false;
            }
            return true;
        }
        public static bool isExclusion(string name)
        {
            name = name.Replace("::", ".");
            name = name.Replace("()", "");
            name = name.Substring(name.IndexOf(" ") + 1);

            for (int i=0; i<name.Split('.').Length + 1; i++)
            {
                foreach (string exclusion in ExclusionBox.Items)
                {
                    if (exclusion.Equals(name))
                        return true;
                }
                if (name.Contains("."))
                    name = name.Remove(name.LastIndexOf('.'));
            }
            return false;
        }
    }
}
