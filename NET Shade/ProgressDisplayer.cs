﻿using System.Windows.Forms;

namespace NET_Shade
{
    class ProgressDisplayer
    {
        internal static ProgressBar ProgressBar { private get; set; }
        private double ProgressBarValue { get; set; }
        private double Step { get; set; }

        public ProgressDisplayer(double step)
        {
            Step = step;
            ProgressBar.Value = 0;
            ProgressBar.Visible = true;
        }

        public void updateProgress()
        {
            ProgressBarValue += Step;
            double difference = ProgressBarValue - ProgressBar.Value;

            if (difference >= ProgressBar.Step)
            {
                for (int i = 0; i < difference / ProgressBar.Step; i++)
                {
                    ProgressBar.PerformStep();
                }
            }
            if (ProgressBar.Value == 100)
            {
                ProgressBar.Visible = false;
            }
        }
    }
}
